import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DashboardComponent }   from './dashboard.component';
import {F1RacersComponent} from "./f1racers.component";
import {F1RacerDetailsComponent} from "./f1-racer-details/f1-racer-details.component";
import {EditComponent} from "./edit.component";
import {NewComponent} from "./new.component";



const routes: Routes = [
  {
    path: 'f1racers',
    component: F1RacersComponent
  },
  {
    path: 'dashboard',
    component: DashboardComponent
  },
  {
    path: '',
    redirectTo: '/dashboard',
    pathMatch: 'full'
  },
  {
    path: 'detail/:number',
    component: F1RacerDetailsComponent
  },
  {
    path:'editor/:number',
    component: EditComponent
  },
  {
    path:'newComponent',
    component: NewComponent
  }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
