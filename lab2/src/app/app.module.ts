import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { F1RacerManagementService} from "./f1-racer-management.service";
import { F1RacersComponent} from './f1racers.component';
import { F1RacerDetailsComponent } from './f1-racer-details/f1-racer-details.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {Alert} from "selenium-webdriver";
import {AlertModule} from "ngx-bootstrap";
import {FilterPipe} from "./filter.pipe";
import {AppComponent} from "./app.component";
import {RouterModule, Routes} from "@angular/router";
import {DashboardComponent} from "./dashboard.component";
import {AppRoutingModule} from "./app-routing.module";
import {EditComponent} from "./edit.component";
import {NewComponent} from "./new.component";



@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    AlertModule.forRoot(),
    AppRoutingModule,
    ReactiveFormsModule
  ],
  exports: [RouterModule],
  declarations: [
    AppComponent,
    F1RacerDetailsComponent,
    FilterPipe,
    F1RacersComponent,
    DashboardComponent,
    EditComponent,
    NewComponent

  ],
  providers: [F1RacerManagementService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
