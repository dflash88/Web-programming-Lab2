import {Component, OnInit} from '@angular/core';
import {F1Racer} from "./model/F1-Racer";
import {F1RacerManagementService} from "./f1-racer-management.service";
import {Router} from "@angular/router";
import {ar} from "ngx-bootstrap/locale";
import {forEach} from "@angular/router/src/utils/collection";
import {F1} from "codelyzer/util/function";

@Component({
  selector: 'my-f1racers',
  templateUrl: './f1racers.component.html',
  styleUrls: ['./f1racers.component.css'],
})
export class F1RacersComponent implements OnInit{
  title = 'F1 grid talk';
  f1racers: Array<F1Racer>;
  f1racersformlocalstorage: F1Racer;
  selectedF1racer: F1Racer;

  onSelect(f1racer: F1Racer): void {
    this.selectedF1racer = f1racer;
  }
  constructor(private router: Router,
              private f1RacersService: F1RacerManagementService){
  }

  ngOnInit():void{
    this.f1RacersService.observedF1Racers.subscribe(f1racers => this.f1racers = f1racers);
    this.allStorage();
    for (var i=0;i<this.f1racers.length-1;i++){
      for (var j=i+1;j<this.f1racers.length;j++)
      {
        if(this.f1racers[i].name==this.f1racers[j].name)
          this.f1racers.splice(j,1);

      }
    }

    //this.f1racers.splice(0,1)

  }


  goCreate(): void {
    this.router.navigate(['/newComponent']);
  }
  allStorage() {

    var values = [],
      keys = Object.keys(localStorage),
      i =0;
    console.log(keys);
    console.log(Object.keys(localStorage).length);

    while ( i<keys.length ) {
      var p = JSON.parse(localStorage.getItem(keys[i]));
      if(p!=null) {
        this.f1racersformlocalstorage=new F1Racer();
        this.f1racersformlocalstorage.name =p.name.toString();
        this.f1racersformlocalstorage.team = p.team.toString();
        this.f1racersformlocalstorage.number = p.number.toString();
        this.f1racersformlocalstorage.id = p.id;
        values.push(localStorage.getItem(keys[i]));
        console.log(p.name);
        this.f1racers.push(this.f1racersformlocalstorage)
        i++;
      }
      //this.f1racers.push(localStorage.getItem(keys[i]).toString());
    }

  }
}
