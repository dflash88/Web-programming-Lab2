import {Component, Input, OnInit} from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Location }                 from '@angular/common';
import 'rxjs/add/operator/switchMap';
import {Router} from "@angular/router";
import {F1RacerManagementService} from "./f1-racer-management.service";
import {F1Racer} from "./model/F1-Racer";

@Component({
  selector: 'editor-racer',
  templateUrl: './editor.component.html',
})
export class EditComponent implements OnInit{

  public CREATE_ACTION = 'Create';
  public EDIT_ACTION = 'Edit';

  private _editingF1Racer: F1Racer;

  private action = this.CREATE_ACTION;
  private isEdit = false;

  protected f1racer: F1Racer;

  @Input('editingF1Racer')
  set setEditingF1Racer(editingF1Racer: F1Racer) {
    console.log('Set editing video');
    this.setF1Racer(editingF1Racer);
  }


  private setF1Racer(editingF1Racer: F1Racer) {
    console.log('Setting F1Racer');
    console.log(editingF1Racer);
    // if the editingVideo is not set, we should prevent the error
    if (editingF1Racer) {
      this.action = this.EDIT_ACTION;
      this.isEdit = true;
      this._editingF1Racer = editingF1Racer;
      this.f1racer.name = editingF1Racer.name;
      this.f1racer.team = editingF1Racer.team;
      this.f1racer.number = editingF1Racer.number;
      this.f1racer.id = editingF1Racer.id;
      console.log("Se setira"+this.f1racer.number)
    }
  }
  constructor(
    private f1racerService: F1RacerManagementService,
    private route: ActivatedRoute,
    private router: Router,
  ) {
    this.f1racer = new F1Racer();
  }

  /*goBack(): void {
    this.router.navigate(['/f1racers']);
  }*/
  goBack(): void {
    this.router.navigate(['/f1racers']);
  }
  ngOnInit() {
    this.route.paramMap
      .switchMap((params: ParamMap) => {
        const f1racerNumber = params.get('number');
        console.info('params', params);
        console.info('number', f1racerNumber);
        const f1RacerPromise = this.f1racerService
          .getF1Racer(f1racerNumber);
        f1RacerPromise.catch(
          error => {
            console.error(error.errorMessage);
          }
        );
        return f1RacerPromise;
      })
      .subscribe(f1racer => {
        console.info('f1racer', f1racer);
        this.setF1Racer(f1racer);
      });
  }

  public edit() {
    this.f1racerService.edit(this._editingF1Racer, this.f1racer)
      .then(f1racerFromServer => this.setF1Racer(f1racerFromServer));
    this.router.navigate(['/f1racers']);

  }



}
